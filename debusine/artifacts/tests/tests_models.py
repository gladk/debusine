# Copyright 2019 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the artifacts application."""

import os
import tempfile
import uuid

from django.core.exceptions import ValidationError
from django.test import TestCase

from debusine.artifacts.models import Artifact, File


class ArtifactTests(TestCase):
    """Unit tests of the ``Artifact`` model."""

    def test_default_values_of_fields(self):
        """Define the fields of the models and test their default values."""
        artifact = Artifact()

        # kind has no default value, but it must be set to be valid
        self.assertEqual(artifact.kind, '')
        artifact.kind = 'sample-type'

        # name is a non-empty string... it defaults to some random UUID.
        self.assertIsInstance(artifact.name, str)
        self.assertNotEqual(artifact.name, '')
        uuid.UUID(artifact.name)  # Ensure it's a valid UUID

        # data is a JSON field defaulting to an empty dict
        self.assertIsInstance(artifact.data, dict)
        self.assertDictEqual(artifact.data, {})

        artifact.clean_fields()
        artifact.save()


class FileTests(TestCase):
    """Unit tests of the ``File`` model."""

    @staticmethod
    def get_new_artifact():
        """Return an ``Artifact`` model instance."""
        a = Artifact(kind='sample-type')
        a.save()
        return a

    @classmethod
    def get_sample_file(cls):
        """Return a valid ``File`` model instance."""
        return File(
            artifact=cls.get_new_artifact(),
            name='sample.txt',
            url='http://example.net/sample.txt',
            path='',
            size=1234,
        )

    def test_default_values_of_fields(self):
        """Define the fields of the model and test their default values."""
        f = File()

        # No artifact assigned by default
        with self.assertRaises(Artifact.DoesNotExist):
            f.artifact
        f.artifact = self.get_new_artifact()

        # name has no default value, but it must be set to be valid
        self.assertEqual(f.name, '')
        f.name = 'sample-file'

        # The content is behind an URL
        self.assertEqual(f.url, '')
        f.url = 'http://example.net/foo.txt'

        # Or behind a local file
        self.assertEqual(f.path, '')
        f.path = '/tmp/foo.txt'

        # But the file has a size
        self.assertIsNone(f.size)

        f.clean_fields()
        f.save()

    def test_url_or_path_must_be_set(self):
        """The model ensures that either the url or the path is non-blank."""
        f = self.get_sample_file()

        f.url = ''
        f.path = ''
        with self.assertRaisesRegex(
                ValidationError,
                'At least one of `url` and `path` must be set.'):
            f.full_clean()

        # Only the URL is enough to validate
        f.url = 'http://example.net'
        f.full_clean()

        # Only the path is enough to validate
        f.url = ''
        f.path = '/tmp/foo.txt'
        f.full_clean()

    def test_size_cannot_be_negative(self):
        """The model ensures that the size is always a positive number."""
        f = self.get_sample_file()
        f.size = -1234
        with self.assertRaises(ValidationError):
            f.full_clean()

    def test_size_is_set_on_save_when_path_exists(self):
        """The model's save method initialize the size from the local file."""
        f = self.get_sample_file()
        data = b'hello'

        tf = tempfile.NamedTemporaryFile(delete=False)
        tf.write(data)
        tf.close()

        f.size = None
        f.path = tf.name
        f.save()
        os.unlink(tf.name)

        self.assertEqual(f.size, len(data))
