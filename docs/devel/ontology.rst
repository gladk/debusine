.. _ontology:

========
Ontology
========

The "type" categorization of artifacts does not enforce anything on the
structure of the associated files and key-value data. However, there must
be some consistency and rules to be able to a make a meaningful use of the
system.

This document presents the various types that we use to manage a
Debian-based distribution. For each type, we explain:

* the expected structure of the "slug" (string identifier)
* what associated files you can find
* what key-value data you can expect
* what relationships with other artifacts are likely to exist

Type ``source-package``
=======================

This artifact represents a set of files that can be extracted in some
way to provide a file hierarchy containing source code that can be built
into ``binary-packages`` artifact(s).

* Slug: *name* _ *version*
* Data:

  * name: the name of the source package
  * version: the version of the source package
  * type: the type of the source package

    * ``dpkg`` for a source package that can be extracted with ``dpkg-source -x`` on the ``.dsc`` file

  * dsc-fields: a parsed version of the fields available in the .dsc file

* Files: for the ``dpkg`` type, a ``.dsc`` file and all the files
  referenced in that file
* Relationships: none

Type ``binary-packages``
========================

This artifact represents the set of binary packages (``.deb`` files and
similar) produced during the build of a source package for a given
architecture.

* Slug: *srcpkg-name* _ *version* _ *architecture*
* Data:

  * srcpkg-name: the name of the source package
  * srcpkg-version: the version of the source package
  * version: the version used for the build (can be different from the
    source version in case of binary-only rebuilds)
  * architecture: the architecture that the packages have been built for.
    Can be any real Debian architecture or ``all``.
  * packages: the list of binary packages that are part of the build

* Files:
* Relationships:

  * built-using: the corresponding ``source-package``
  * built-using: other ``binary-packages`` (for example in the case of
    signed packages duplicating the content of an unsigned package)
  * built-using: other ``source-package`` (general case of Debian's
    ``Built-Using`` field)

Type ``source-upload``
======================

* Slug: random uuid
* Data:

  * type: the type of the source upload

    * ``dpkg``: for an upload generated out of a ``.changes`` file created
      by ``dpkg-buildpackage``

  * repository: the target repository (corresponds to the ``Distribution`` field
    from the usual ``.changes`` file)
  * changes-fields: a parsed version of the fields available in the
    ``.changes`` file

* Files:

  * a ``.changes`` file

* Relationships:

  * extends: one ``source-package``

Type ``binary-upload``
======================

* Slug: random uuid
* Data:

  * type: the type of the source upload

    * ``dpkg``: for an upload generated out of a ``.changes`` file created
      by ``dpkg-buildpackage``

  * repository: the target repository (corresponds to the ``Distribution`` field
    from the usual ``.changes`` file)
  * changes-fields: a parsed version of the fields available in the
    ``.changes`` file

* Files:

  * a ``.changes`` file

* Relationships:

  * extends: one (or more) ``binary-packages``

Type ``repository``
===================

Represents an APT repository.

* Slug: *codename* or (*origin* : *codename* if we have to handle multiple vendors)
* Data:

  * base-url: the base url of a repository (eg "http://deb.debian.org/debian")
  * codename: the codename of the repository (eg "sid")
  * possibly, other data extracted from the ``InRelease`` file:

    * components: list of components available
    * architectures: list of architectures available
    * origin: string identifier for the repository owner

* Files:

  * Multiple JSON encoded files making it easy to browse the content of
    the repository. Exact format to be determined.

* Relationships:

  * extends: (optional) another ``repository`` (e.g. "experimental"
    extends "unstable")
  * includes: many ``binary-packages`` and ``source-package``
